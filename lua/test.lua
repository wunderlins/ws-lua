-- this is the ini code, print will output tothe server's stdout
print("hello")
print(wss._FD)

--[[
logging

This functions logs to the module log `log/submodule_lua.log`.

The following log levels are available:
- wss.FATAL
- wss.ERROR
- wss.WARN
- wss.INFO
- wss.DEBUG
- wss.TRACE

]]--
wss.log(wss.INFO, "Loading lua script test.lua")

--[[
for i, v in pairs(debug.getinfo(1)) do
	print(i .. ' ' .. v)
end
]]--

--[[
/**
 * C API - The subprotocol API calls
 */
 typedef void (*onConnect)(int fd, char *ip, int port, char *path, char *cookies);
 typedef void (*onMessage)(int fd, wss_opcode_t opcode, char *message, size_t message_length);
 typedef void (*onWrite)(int fd, char *message, size_t message_length);
 typedef void (*onClose)(int fd);
 typedef void (*onDestroy)();
]]--

--[[
    LUA API
]]--

--[[
Called once when a new connection is established

@param ip String ip address of the client
@param port Int port of the client
@param path String Path part request URI
@param cookies String cookies
@return void FIXME: need to update api to be able to respond with errors
]]--
function onConnect(ip, port, path, cookies)
    wss.log(wss.INFO, "onConnect("  ..ip.. ", " ..port.. ", " ..path.. ", " ..cookies .. ")")
    return
end

--[[
Handle an incoming message fro mthe client

@param opcode Int RFC6455 opcode https://datatracker.ietf.org/doc/html/rfc6455#page-65
@param message String/byte array the message content
@param length Int length of the message
@return void FIXME: need to update api to be able to respond with errors
]]--
function onMessage(opcode, message, length)
    wss.log(wss.INFO, "onMessage("  ..opcode.. ", " ..message.. ", " ..length.. ")")
    wss.send(opcode, message, length)
    return
end