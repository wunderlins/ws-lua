#include <stdlib.h>
#include <pthread.h>
#include <errno.h> 			    /* errno */
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

#include "../../include/log.h"
#include "../broadcast/predict.h"
#include "../broadcast/uthash.h"
#include "lua.h"

// lua api ////////////////////////////////////////////////////////////////////
/**
 * A hashtable containing all active lua interpresters
 * 
 * https://troydhanson.github.io/uthash/userguide.html
 */
lua_handle_t * volatile luad = NULL;

/**
 * a list of lua files
 * 
 * these file names are the lua endpoints. If /script is called, 
 * The uri will be mapped to $script_dir/script.lua.
 * 
 * These files are indext onInit from the lua_dir and the 
 * request uri (sans *lua suffix) is mapped to the these names.
 */
lua_files_t * volatile  lua_files = NULL;

/**
 * A lock that ensures the hash table is update atomically
 */
pthread_rwlock_t lock;

/**
 * file handle for log file
 */
static FILE *file;

/**
 * sub module config, script directory
 */
static char script_dir[PATH_MAX] = "lua/";

/**
 * memory allcoators
 */
typedef struct {
    void *(*malloc)(size_t);
    void *(*realloc)(void *, size_t);
    void (*free)(void *);
} allocators;

allocators allocs = {
    malloc,
    realloc,
    free
};

WSS_send send = NULL;

static char *endpoint = NULL;

/*
static int c_swap (lua_State *L) {
    //check and fetch the arguments
    double arg1 = luaL_checknumber (L, 1);
    double arg2 = luaL_checknumber (L, 2);

    //push the results
    lua_pushnumber(L, arg2);
    lua_pushnumber(L, arg1);

    //return number of results
    return 2;
}

static int my_sin (lua_State *L) {
    double arg = luaL_checknumber (L, 1);
    lua_pushnumber(L, sin(arg));
    return 1;
}
*/
static int lua_ext_log(lua_State *L) {
    // get the line number
    // https://stackoverflow.com/questions/2555856/current-line-number-in-lua
    lua_Debug ar;
    lua_getstack(L, 1, &ar);
    lua_getinfo(L, "nSl", &ar);
    int line = ar.currentline;
    int level = (int) luaL_checknumber (L, 1);
    const char* message = luaL_checkstring(L, 2);
    
    log_log(level, ar.short_src, line, message);

    return 0;
}

static int lua_ext_send(lua_State *L) {
    int opcode = (int) luaL_checknumber(L, 1);
    const char* message = luaL_checkstring(L, 2);
    int message_length = (int) luaL_checknumber(L, 3);

    // get the file handle, FIXME: this is dangerous because it's user mdoifiable
    lua_getglobal(L, "__FD"); 
    int fd = lua_tointeger(L, -1);
    lua_pop(L, 1);  // pop returned value off the stack

    send(fd, opcode, (char*) message, message_length);

    return 0;
}

//library to be registered
static const struct luaL_Reg lua_wss_lib [] = {
      //{"c_swap", c_swap},
      //{"mysin", my_sin}, /* names can be different */
      {"log", lua_ext_log},
      {"send", lua_ext_send},
      {NULL, NULL}  /* sentinel */
    };

//name of this function is not flexible
int luaopen_wss(lua_State *L, int fd){

    // We create a new table
    lua_newtable(L);

    // register log levels
    /*
    WSS_LOG_FATAL,
    WSS_LOG_ERROR,
    WSS_LOG_WARN,
    WSS_LOG_INFO,
    WSS_LOG_DEBUG,
    WSS_LOG_TRACE
    */

    lua_createtable(L, 0, 4);
    // log levels
    lua_pushstring(L, "FATAL"); lua_pushinteger(L, 0); lua_settable(L, -3);
    lua_pushstring(L, "ERROR"); lua_pushinteger(L, 1); lua_settable(L, -3);
    lua_pushstring(L, "WARN");  lua_pushinteger(L, 2); lua_settable(L, -3);
    lua_pushstring(L, "INFO");  lua_pushinteger(L, 3); lua_settable(L, -3);
    lua_pushstring(L, "DEBUG"); lua_pushinteger(L, 4); lua_settable(L, -3);
    lua_pushstring(L, "TRACE"); lua_pushinteger(L, 5); lua_settable(L, -3);

    // file descriptor of this connection
    lua_pushstring(L, "_FD");   lua_pushinteger(L, fd); lua_settable(L, -3);

    // Here we set all functions from lua_wss_lib array into
    // the table on the top of the stack
    luaL_setfuncs(L, lua_wss_lib, 0);
    
    // We get the table and set as global variable, alle elements are now available
    // at `wss.func/var` in the lua script
    lua_setglobal(L, "wss");

    // easy access to FD for write
    lua_pushinteger(L, fd);
    lua_setglobal(L, "__FD");

    return 1;
}

/**
 * get a new instance of the lua interpreter
 * 
 * @return 	            [lua_State*] handle to lua interpreter
 */
lua_State *luainit(int fd) {
    lua_State *L;
    L = luaL_newstate();
    luaL_openlibs(L);
    luaopen_wss(L, fd);

    return L;
}

lua_State *luaget(int fd) {
    lua_handle_t *luah;

    if ( unlikely(pthread_rwlock_wrlock(&lock) != 0) ) {
        return NULL;
    }

    HASH_FIND_INT(luad, &fd, luah);
    pthread_rwlock_unlock(&lock);

    // this is the lua interpreter of this thread
    lua_State *L = luah->luaL;

    return L;
}
// lua api ////////////////////////////////////////////////////////////////////

/**
 * Event called when subprotocol is initialized.
 *
 * @param 	sub_config  [char *]            "The configuration of the subprotocol"
 * @param 	s	        [WSS_send]          "Function that send message to a single recipient"
 * @return 	            [void]
 */
void onInit(char *config, WSS_send s) {
    send = s;

    // initialize sub protocol logger
    if ( NULL == (file = fopen("log/submodule_lua.log", "a+")) ) {
        fprintf(stderr, "%s\n", strerror(errno));
        return;
    }

    // Set file to write log to
    log_set_fp(file);

    // Set log level to default value until configuration is loaded
    log_set_level(WSS_LOG_TRACE);

    // log call
    WSS_log_debug("lua::onInit(%s, %d)", config, s);

    // read config, script_dir is the only key we support
    char new_dir[PATH_MAX] = {0};
    char key[50] = "script_dir";
    char *p = strstr(config, key);
    int i = 0;
    int length = strlen(config);
    int using_default_dir = 1;
    if (p) {
        i = 10; // shift over key
        // find equal
        while (p[i] != '=' && i < length) i++;
        i++;
        // skip all white space, just in case
        while ((p[i] == ' ' || p[i] == '\t') && i < length) i++;
        
        if (i < length) {
            int ii = 0;
            while (i < length && p[i] != '0' && p[i] != ';') {
                new_dir[ii++] = p[i++];
            }
            if (new_dir[ii-1] != '/')
                new_dir[ii++] = '/';
            new_dir[ii] = 0;

            WSS_log_info("lua::onInit, using script_dir: '%s'", new_dir);
            strcpy(script_dir, new_dir);
            using_default_dir = 0;
        }
    }

    if (using_default_dir == 1)
        WSS_log_info("lua::onInit, using DEFAULT script_dir: '%s'", script_dir);
    
    // check if the dir exists
    struct stat stat_dir = {0};
    if (stat(script_dir, &stat_dir) == -1) {
        WSS_log_error("Error: %s '%s'", strerror(errno), script_dir);
        WSS_log_error("Aborting.");
        exit(10);
    }

    // find all scripts files ending in *.lua within script_dir ( no sub directory scan)
    struct dirent *entry;
    DIR *dp;

    dp = opendir(script_dir);
    if (dp == NULL) {
        WSS_log_error("opendir: Path does not exist or could not be read.");
        WSS_log_error("Aborting.");
        exit(11);
    }

    // find all files with a name ending in "*.lua"
    // store them in a linked list lua_files
    if ( unlikely(pthread_rwlock_wrlock(&lock) != 0) ) {
        return;
    }

    while ((entry = readdir(dp))) {
        int length = strlen(entry->d_name);
        if (length < 4) continue;
        char *cmp = (entry->d_name) + length - 4;
        WSS_log_trace("lua::onInit, %s", cmp);
        if (length > 4 && strcmp(cmp, ".lua") != 0) continue;
        endpoint = allocs.malloc(length-3);
        memcpy(endpoint, &entry->d_name, length-4);
        endpoint[length-4] = '\0';
        WSS_log_info("lua::onInit, enabling lua endpoint /%s (%d)", endpoint, entry->d_reclen);

        lua_files_t *f;
        if ( unlikely(NULL == (f = (lua_files_t *) allocs.malloc(sizeof(lua_files_t)))) ) {
            pthread_rwlock_unlock(&lock);
            closedir(dp);
            return;
        }

        f->name = endpoint;
        LL_PREPEND(lua_files, f);
    }

    pthread_rwlock_unlock(&lock);

    closedir(dp);

    return;
}

/**
 * Sets the allocators to use instead of the default ones
 *
 * @param 	submalloc	[WSS_malloc_t]     "The malloc function"
 * @param 	subrealloc	[WSS_realloc_t]    "The realloc function"
 * @param 	subfree	    [WSS_free_t]       "The free function"
 * @return 	            [void]
 */
void setAllocators(WSS_malloc_t submalloc, WSS_realloc_t subrealloc, WSS_free_t subfree) {
    allocs.malloc = submalloc;
    allocs.realloc = subrealloc;
    allocs.free = subfree;
}

/**
 * Function that finds a lua interpreter using the filedescriptor of the client.
 *
 * @param 	fd 	[int] 		        "The filedescriptor associated to some client"
 * @return 		[lua_State *] 	    "Returns the Lua handle if successful, otherwise NULL"
 */
inline static lua_handle_t *lua_handle_find(int fd) {
    lua_handle_t *luah = NULL;

    if ( unlikely(pthread_rwlock_rdlock(&lock) != 0) ) {
        return NULL;
    }

    HASH_FIND_INT(luad, &fd, luah);

    pthread_rwlock_unlock(&lock);

    return luah;
}

/**
 * Event called when a new session has handshaked and hence connects to the WSS server.
 *
 * @param 	fd	     [int]     "A filedescriptor of a connecting session"
 * @param 	ip       [char *]  "The ip address of the connecting session"
 * @param 	port     [int]     "The port of the connecting session"
 * @param 	path     [char *]  "The connection path. This can hold HTTP parameters such as access_token, csrf_token etc. that can be used to authentication"
 * @param 	cookies  [char *]  "The cookies received from the client. This can be used to do authentication."
 * @return 	         [void]
 */
void onConnect(int fd, char *ip, int port, char *path, char *cookies) {
    WSS_log_debug("lua::onConnect(%d, %s, %d, %s, %s)", fd, ip, port, path, cookies);

    // check if endpoint exists
    lua_files_t *elt, *tmpf;
    char *endpoint = path+1; // remove leading slash
    int found = 0; 
    LL_FOREACH_SAFE(lua_files, elt, tmpf) {
        if (strcmp(elt->name, endpoint) == 0) {
            WSS_log_debug("lua::onConnect, endpoint '%s' found", endpoint);
            found = 1;
            break;
        }
    }

    if (found == 0) {
        WSS_log_error("lua::onConnect, endpoint '$script_dir/%s.lua' NOT found", endpoint);
        return;
    }

    lua_handle_t *luah;

    // setup lua handle
    if ( likely(NULL != (luah = lua_handle_find(fd))) ) {
        WSS_log_error("Found lua handle where there should be none.");
        return;
    }

    if ( unlikely(pthread_rwlock_wrlock(&lock) != 0) ) {
        WSS_log_error("Failed to get lock.");
        return;
    }

    if ( unlikely(NULL == (luah = (lua_handle_t *) allocs.malloc(sizeof(lua_handle_t)))) ) {
        WSS_log_error("Failed to allocate memory for lua handle.");
        pthread_rwlock_unlock(&lock);
        return;
    }

    luah->fd = fd;
    luah->luaL = luainit(fd);
    luah->path = allocs.malloc(strlen(endpoint)+1);
    luah->valid_script_file = 0;
    memcpy(luah->path, endpoint, strlen(endpoint)+1);

    HASH_ADD_INT(luad, fd, luah);

    pthread_rwlock_unlock(&lock);
    
    // setup lua environment
    lua_pushinteger(luah->luaL, fd);
    lua_setglobal(luah->luaL, "_FD");
    
    // run script (init code)
    char lua_file[PATH_MAX] = {0};
    strcat(lua_file, script_dir);
    strcat(lua_file, endpoint);
    strcat(lua_file, ".lua");
    WSS_log_debug("Running lua script '%s'", lua_file);
    if (luaL_dofile(luah->luaL, lua_file) == LUA_OK) {
        lua_pop(luah->luaL, lua_gettop(luah->luaL));
    } else {
        WSS_log_error("Failed to run script '%s'", lua_file);
        return;
    }

    // remeber that the script file could be parsed and is initialized
    luah->valid_script_file = 1;

    // time to call the onConnect function of the script
    // push functions and arguments
    lua_settop(luah->luaL, 0); // reset the stack, just to make sure, it should be empty
    lua_getglobal(luah->luaL, "onConnect");  // function to be called 
    if (lua_isnone(luah->luaL, -1)) { // function is not defined
        WSS_log_info("no onConnect() handler found");
        lua_settop(luah->luaL, -1);    // get lost of the return value
        return;
    }
    // set parameters
    lua_pushstring(luah->luaL, ip);      // 1ts function argument, string ip
    lua_pushinteger(luah->luaL, port);   // 2nd function argument, int port
    lua_pushstring(luah->luaL, path);    // 3rd function argument, string request uri
    lua_pushstring(luah->luaL, (cookies == NULL) ? "" : cookies); // ath function argument, string raw cookie string

    // do the function call with 4 arguments and 0 return values (void)
    int pcall_ret = lua_pcall(luah->luaL, 4, 0, 0);
    if (pcall_ret != 0) {
        // error(L, "error running function `f': %s",
        //            lua_tostring(luah->luaL, -1));
        WSS_log_error("Error onConnect(): %d %s", pcall_ret, lua_tostring(luah->luaL, -1));
        lua_settop(luah->luaL, -1);    // get lost of the return value
        return;
    }

    return;
}

/**
 * Event called when a session has received new data.
 *
 * @param 	fd	            [int]     "A filedescriptor of the session receiving the data"
 * @param 	message	        [char *]  "The message received"
 * @param 	message_length	[size_t]  "The length of the message"
 * @return 	                [void]
 */
void onMessage(int fd, wss_opcode_t opcode, char *message, size_t message_length) {
    WSS_log_debug("lua::onMessage(%d, %d, %s, %d)", fd, opcode, message, message_length);

    lua_handle_t *luah;

    if (NULL == (luah = lua_handle_find(fd)) ) {
        WSS_log_error("Failed to get lua handle.");
        return;
    }

    if (!luah->valid_script_file) {
        WSS_log_error("Error in script, cannot run onMessage() handler.");
        return;
    }
    
    // time to call the onConnect function of the script
    // push functions and arguments
    lua_settop(luah->luaL, 0); // reset the stack, just to make sure, it should be empty
    lua_getglobal(luah->luaL, "onMessage");  // function to be called 
    if (lua_isnone(luah->luaL, -1)) { // function is not defined
        WSS_log_info("no onMessage() handler found");
        lua_settop(luah->luaL, -1);    // get lost of the return value
        return;
    }
    // set parameters
    lua_pushinteger(luah->luaL, opcode);         // 1ts function argument, int opcode
    lua_pushstring(luah->luaL, message);         // 2nd function argument, string message
    lua_pushinteger(luah->luaL, message_length); // 3rd function argument, int message length

    // do the function call with 3 arguments and 0 return values (void)
    int pcall_ret = lua_pcall(luah->luaL, 3, 0, 0);
    if (pcall_ret != 0) {
        WSS_log_error("Error onMessage(): %d %s", pcall_ret, lua_tostring(luah->luaL, -1));
        lua_settop(luah->luaL, -1);    // get lost of the return value
        return;
    }

    //send(fd, opcode, message, message_length);
    return;
}

/**
 * Event called when a session are about to perform a write.
 *
 * @param 	fd	            [int]     "A filedescriptor the session about to receive the message"
 * @param 	message	        [char *]  "The message that should be sent"
 * @param 	message_length	[size_t]  "The length of the message"
 * @return 	                [void]
 */
void onWrite(int fd, char *message, size_t message_length) {
    WSS_log_debug("lua::onWrite(%d, %s, %d)", fd, message, message_length);
    return;
}

/**
 * Event called when a session disconnects from the WSS server.
 *
 * @param 	fd	[int]     "A filedescriptor of the disconnecting session"
 * @return 	    [void]
 */
void onClose(int fd) {
    WSS_log_debug("lua::onClose(%d)", fd);
    lua_handle_t *luah;

    if (NULL == (luah = lua_handle_find(fd)) ) {
        WSS_log_error("Failed to get lua handle.");
        return;
    }

    if ( unlikely(pthread_rwlock_wrlock(&lock) != 0) ) {
        WSS_log_error("Failed to get lock.");
        return;
    }

    if (!luah->valid_script_file) {
        WSS_log_error("Error in script, cannot run onClose() handler.");
        return;
    }
    
    lua_close(luah->luaL); // close and free lua handle
    allocs.free(luah->path);
    HASH_DEL(luad, luah);
    allocs.free(luah);

    pthread_rwlock_unlock(&lock);

    return;
}

/**
 * Event called when the subprotocol should be destroyed.
 *
 * @return 	    [void]
 */
void onDestroy() {
    WSS_log_debug("lua::onDestroy()");
    lua_handle_t *luah, *tmp;

    if ( unlikely(pthread_rwlock_wrlock(&lock) != 0) ) {
        return;
    }

    // destroy all lua handles
    HASH_ITER(hh, luad, luah, tmp) {
        if (luah != NULL) {
            lua_close(luah->luaL); // close and free lua handle
            allocs.free(luah->path);
            HASH_DEL(luad, luah);
            allocs.free(luah);
        }
    }

    // destroy al lendpoints
    lua_files_t *elt, *tmpf;
    LL_FOREACH_SAFE(lua_files, elt, tmpf) {
        LL_DELETE(lua_files, elt);
        allocs.free(elt);
    }

    pthread_rwlock_unlock(&lock);
    pthread_rwlock_destroy(&lock);
}
